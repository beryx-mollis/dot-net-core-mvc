using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
// See https://aka.ms/new-console-template for more information
// 1.图形类：
//   求周长，面积（三角形， 四边形， 圆形）
namespace task
{
    public class square
    {
        private int x;
        private int y;
        private int h;
        private int r;
        public square(int x, int y, int h, int r)
        {
            this.x = x;
            this.y = y;
            this.h = h;
            this.r = r;
        }
        public void area()
        {
            Console.WriteLine($"四边形的面积:{x * y}");
            Console.WriteLine($"三角形的面积:{x * h / 2}");
            Console.WriteLine($"圆形的面积:{r * r * 3.14}");
        }
        public void perimeter()
        {
            Console.WriteLine($"四边形的周长:{(x + y) * 2}");
            Console.WriteLine($"三角形的周长:{x + y + r}");
            Console.WriteLine($"圆形的周长:{2 * r * 3.14}");
        }
    }
    //  2.员工类Employee
    //  字段：姓名，工作年限，月薪、

    // 组长： 月薪+1000*年限
    // 经理 ：月薪+1000*年限    字段：基础分红1000
    // 客户经理：月薪+1000*年限*分红(基础分红*3)

    // 求组长，经理的年薪。
    public class Employee
    {
        public void leader(string name, int year, int msalary)
        {
            Console.WriteLine($"{name}:{msalary + 1000 * year}元");
        }
        public void manager(string name, int year, int msalary)
        {
            Console.WriteLine($"{name}:{msalary + 1000 * year}元");
        }
    }
    }