using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mvc1.Models;
namespace mvc1.Models
{
    public class Player
    {
        public int Number{get;set;}
        public string Name{get;set;}
    }
}