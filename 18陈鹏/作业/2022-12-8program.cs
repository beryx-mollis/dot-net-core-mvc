// See https://aka.ms/new-console-template for more information
// 1，	定义一个有抽象方法display()的类SuperClass以及提供不同实现方法的子类SubClassA和SubClassB，并创建一个测试类PolyTester，分别创建SubClassA和SubClassB的对象。调用每个对象的display()。 要求：输出结果为： display A、display B
// 2，	按要求编写C#程序：
// （1）编写一个接口：InterfaceA，只含有一个方法int Method(int n)；
// （2）编写一个类：ClassA来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算1到n的和；
// （3）编写另一个类：ClassB来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算n的阶乘（n!）；
// （4）编写main方法中使用接口回调的形式来测试实现接口的类。

namespace test
{
    interface InterfaceA
    {
        int Method(int n);
    }
    public class ClassA : InterfaceA
    {
        public int Method(int n)
        {
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            return sum;
        }
    }
    public class ClassB : InterfaceA
    {
        public int Method(int n)
        {
            int sum = 1;
            for (int i = 1; i <= n; i++)
            {
                sum *= i;
            }
            return sum;
        }
    }
}