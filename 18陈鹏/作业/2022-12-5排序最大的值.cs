// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
//1.逆序输出：9  2 5 7 8  --> 8 7 5 2 9
int[] arr = { 9, 2, 5, 7, 8 };
reverse(arr);
static void reverse(int[] arr)
{
    int number = 0;
    for (int i = 0; i < arr.Length / 2; i++)
    {
        number = arr[i];
        arr[i] = arr[arr.Length - i - 1];
        arr[arr.Length - i - 1] = number;
    }
    foreach (var e in arr)
    {
        Console.WriteLine(e);
    }
}
//2. 求最大最小值{1, -1, 5, 6, 8, 101}
int[] arr1 = { 1, -1, 5, 6, 8, 101 };
static void maxMin(int[] arr)
{
    int max = arr[1];
    int min = arr[1];
    for (int i = 0; i < arr.Length; i++)
    {
        if (arr[i] > max)
        {
            max = arr[i];
        }
        if (arr[i] < min)
        {
            min = arr[i];
        }
    }
    Console.WriteLine($"最大值:{max}最小值:{min}");
}
maxMin(arr1);
//3. a=9 b=5 c=6  --> 5 6 9
int a = 9; int b = 5; int c = 6;
static void sort(int a, int b, int c)
{
    int number = 0;
    if (a > b)
    {
        number = a;
        a = b;
        b = number;
    }
    if (b > c)
    {
        number = b;
        b = c;
        c = number;
    }
    Console.WriteLine($"{a} {b} {c}");
}
sort(a, b, c);