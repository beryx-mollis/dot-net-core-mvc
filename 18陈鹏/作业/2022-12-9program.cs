using test;
List<Student> list = new List<Student>()
    {
        new Student() { ID = 3, Name = "淑芳" },
        new Student() { ID = 1, Name = "陈鹏" },
        new Student() { ID = 2, Name = "立智" }
    };
for (int i = 0; i < list.Count; i++)
{
    for (int j = 0; j < list.Count; j++)
    {
        if (list[j].ID > list[i].ID)
        {
            var index = list[i];
            list[i] = list[j];
            list[j] = index;
        }
    }
}
foreach (var item in list)
{
    Console.WriteLine(item.ID + "  " + item.Name);
}