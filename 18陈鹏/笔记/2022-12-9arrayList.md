动态数组ArrayList：

---                                                    
增：
数组名.add(元素)
指定位置添加元素
数组名.Insert(下标,元素)

删:
数组名.remove(元素)
根据下标删除元素
数组名.removeAt(下标)

查：
数组名.IndexOf(元素)
---

引用传递ref out	(ref:在执行引用传递之前，必须要初始化（赋值）， out：可以只声明，不赋值)

---
//引用
static void a(ref(out) int a,ref(out) int b)
{

}
---