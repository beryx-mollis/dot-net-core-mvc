o/r m框架(EntityFrameworkcore)能实现将整个数据库托管到程序中来进行封装（类），通过操作这些类，可以完成对数。
DBfirst:数据库优先(先有数据库，再有代码)
**CodeFirst**:代码优先(以逻辑代码生成一个数据库)
Dbcontext:上下文(连接数据库与代码的渠道)，属于：efcore框架中的类

1·引入EFcore:dotnet add package(添加包/库)  **Microsoft.EntityFrameworkcore**
2.引入数据库：dotnet add package Microsoft.EntityFrameworkCore.Sqlserver
virtual（虚方法）：可重写可不重写（不用这个方法）
//重写：签名需要一致：返回类型，访问级别，参数,方法名